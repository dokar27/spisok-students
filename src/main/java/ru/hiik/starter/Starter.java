/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik.starter;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.transaction.Transactional;
import ru.hiik.student.Student;

/**
 *
 * @author student
 */
@ApplicationScoped
public class Starter {

    private static final Logger LOG = Logger.getLogger(Starter.class.getName());

    void onStart(@Observes StartupEvent ev) {
        LOG.info("Сервер стартовал");
        createStudents();
    }

    void onStop(@Observes ShutdownEvent ev) {
        LOG.info("Сервер отстановился");
    }

    @Transactional
    public void createStudents() {
        Student student1 = new Student();
        student1.firstName = "Виктор";
        student1.lastName = "Козлов";
        student1.middleName = "Сергеевич";
        student1.course = "2";
        student1.studentGroup = "ПОВТ-20д";
        student1.persistAndFlush();
        System.out.println("Создан студент");
        Student student2 = new Student();
        student2.firstName = "Сергей";
        student2.lastName = "Фузеев";
        student2.middleName = "Алексеевич";
        student2.course = "2";
        student2.studentGroup = "ПОВТ-20д";
        student2.persistAndFlush();
        System.out.println("Создан студент 2");
        Student student3 = new Student();
        student3.firstName = "Софья";
        student3.lastName = "Олейник";
        student3.middleName = "Андреевна";
        student3.course = "2";
        student3.studentGroup = "ПОВТ-20д";
        student3.persistAndFlush();
        System.out.println("Создан студент 3");

    }

}
